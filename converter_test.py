from converter import convert


def test_c2f_45():
    assert convert('c', 'f', 45) == 113.0

def test_f2c_45():
    assert convert('f', 'c', 45) == 7.222222222222222

def test_c2k_45():
    assert convert('c', 'k', 45) == 318.15

def test_k2c_45():
    assert convert('k', 'c', 45) == -228.14999999999998

def test_f2k_45():
    assert convert('f', 'k', 45) == 280.3722222222222

def test_k2f_45():
    assert convert('k', 'f', 45) == -134.54999999999998

def test_c2f_k_c_1():
    assert convert('c', 'f', 100) == 212.0
def test_c2f_k_c_2():
    assert convert('c', 'k', 100) == 373.15
def test_c2f_k_c_3():
    assert convert('c', 'c', 100) == 100
def test_f2f_k_c_1():
    assert convert('f', 'c', 100) == 37.77777777777778
def test_f2f_k_c_2():
    assert convert('f', 'k', 100) == 310.92777777777775
def test_f2f_k_c_3():
    assert convert('f', 'f', 100) == 100

def test_k2f_k_c_1():
    assert convert('k', 'c', 100) == -173.14999999999998
def test_k2f_k_c_2():
    assert convert('k', 'f', 100) == -35.54999999999998
def test_k2f_k_c_3():
    assert convert('k', 'k', 100) == 100

def test_syntax_error():
    assert convert(100, 'f', 21) == "Данные не верны"
