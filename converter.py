def convert(a, b, count):
    if type(a) != str or type(b) != str:
        return ('Данные не верны')
    if type(count) != int:
        return ('Данные не верны')
    a = a.lower()
    b = b.lower()
    if a == 'c' and b == 'f':
        return (count * 9/5) + 32
    elif a == 'c' and b == 'k':
        return 273.15 + count
    elif a == 'c' and b == 'c':
        return count
    elif a == 'f' and b == 'c':
        return (count - 32) * 5/9
    elif a == 'f' and b == 'k':
        return 273.15 + ((count - 32) * (5/9))
    elif a == 'f' and b == 'f':
        return count
    elif a == 'k' and b == 'c':
        return count - 273.15
    elif a == 'k' and b == 'f':
        return ((count + 32) * (9/5)) - 273.15
    elif a == 'k' and b == 'k':
        return count
    else:
        return ('Данные не верны')
if __name__ == "__main__":
    fr = input()
    fa = input()
    while True:
        try:
            temp = int(input())
        except ValueError:
            print("Error! Это не число, попробуйте снова.")
        else:
            break

    
    print(convert(fr,fa,temp))
